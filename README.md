# Terraform README
This README provides an overview and explanation of the Terraform code in this repository.




## Introduction
Terraform code for provisioning and managing infrastructure resources can be found in this repository. The code is divided into modules, each of which is in charge of a certain resource or component.


## Terraform installed on your local machine
Puppetmaster operating in an AWS EC2 instance or virtual machine
Access to the cloud provider where the infrastructure will be provided, such as AWS or Azure
appropriate access credentials and authentication for the cloud provider





## Clone this repository to your local machine.
- Open the relevant directory where the Terraform code is located.
- Run terraform init to initialise the Terraform working directory.
- Run Terraform plan to review the execution plan.
To apply the modifications, run terraform apply.
- In the interim, paste the init.pp file into the puppet server's manifests directory.
- Sign the puppet agent certificate from the puppet master instance once step 5 is finished.



## Usage
A fresh EC2 instance will be provisioned by the terraform code, which will also automatically install Puppet Agent on it. Following installation, the Puppet Agent service will launch and use the Puppet-Server catalogue.


